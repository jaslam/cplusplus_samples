// CPlusPlus_Samples.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Celebrity.h"
#include "CardDeck.h"
#include "BinarySearchTree.h"
#include "LinkedList.h"
#include "Stack.h" 
#include "Utils.h"
#include "Strings.h"
#include "Trie.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	//Celebrity_RunTests();
	//CardDeck::RunTests();
	//BinarySearchTree::RunTests();
	//SinglyLinkedList<int>::RunTests();
	//CircularLinkedList<int>::RunTests();
	//Utils::RunTests();
	//Stack<int>::RunTests();
	//Strings::RunTests();
	Trie::RunTests();

	cout << endl << endl << ">>>>> Exiting now ..." << endl << endl;
	_getch();

	return 0;
}

