#pragma once


struct BTNode
{
	int m_data;
	BTNode* m_pLeft;
	BTNode* m_pRight;

	BTNode(int data) :
		m_data(data)
		, m_pLeft(NULL)
		, m_pRight(NULL)
	{
	}
};

/**
* Binary Tree.
*/
class BinaryTree
{
private:
	BTNode* m_pRoot;

	// recursive functions
	void preOrderTraversal(BTNode* pNode);
	void inOrderTraversal(BTNode* pNode);
	void postOrderTraversal(BTNode* pNode);


public:
	BinaryTree() :
		m_pRoot(NULL)
	{
	}

	BTNode* getRoot() {
		return m_pRoot;
	}

	void setRoot(BTNode* pNode) {
		delete m_pRoot;
		m_pRoot = pNode;
	}

	// depth first traversals
	void preOrderTraversal();
	void inOrderTraversal();
	void postOrderTraversal();

	void preOrderTraversalIterative();
	void inOrderTraversalIterative();
	void postOrderTraversalIterative();


	// breadth first search
};
