#pragma once

template<class T>
class Stack
{
public:
	virtual void push(T data) = 0;
	virtual T pop() = 0;
	virtual T peek() = 0;
	virtual bool isEmpty() = 0;

	static void RunTests();
};

template<class T>
class StackUsingArray : public Stack<T>
{
private:
	T m_array[64];
	size_t m_nTop;

public:
	StackUsingArray<T>() :
		m_nTop(-1)
	{
	}

	void push(T data)
	{
		if (m_nTop < 64) {
			m_array[m_nTop++] = data;
		}
	}

	T pop()
	{
		return m_array[m_nTop--];
	}

	T peek()
	{
		return m_array[m_nTop];
	}

	bool isEmpty()
	{
		return m_nTop == -1;
	}

};

template<class T>
void Stack<T>::RunTests()
{
	cout << endl << endl << ">>>>> Running Stack tests ...." << endl;

	StackUsingArray<int> st;
	st.push(10);
	st.push(20);
	st.push(5);
	st.push(100);
	st.push(65);
	cout << endl << "st.top = " << st.peek();
	st.pop();
	st.pop();
	cout << endl << "st.top = " << st.peek();

}

