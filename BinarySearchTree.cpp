// BinaryTree.cpp

#include "stdafx.h"

#include "BinarySearchTree.h"


using namespace std;


void BinarySearchTree::insert(BTNode* pNode, int data)
{
	if (data <= pNode->m_data) {
		if (pNode->m_pLeft) {
			insert(pNode->m_pLeft, data);
		}
		else {
			pNode->m_pLeft = new BTNode(data);
			return;
		}
	}
	else {
		if (pNode->m_pRight) {
			insert(pNode->m_pRight, data);
		}
		else {
			pNode->m_pRight = new BTNode(data);
			return;
		}
	}
}

void BinarySearchTree::insert(int data)
{
	if (getRoot() == NULL) {
		setRoot(new BTNode(data));
		return;
	}

	insert(getRoot(), data);
}


/**
				10
			5			20
		7			15		18
	6		8			16		19
				
 */

void BinarySearchTree::RunTests()
{
	cout << endl << ">>>>> Running BinarySearchTree tests ...." << endl;
	BinarySearchTree bst;

	bst.insert(30);
	bst.insert(20);
	bst.insert(10);
	bst.insert(5);
	bst.insert(15);
	bst.insert(40);
	bst.insert(35);
	bst.insert(38);
	bst.insert(45);

	bst.preOrderTraversal();
	bst.preOrderTraversalIterative();
	bst.inOrderTraversal();
	bst.inOrderTraversalIterative();
	bst.postOrderTraversal();
}
