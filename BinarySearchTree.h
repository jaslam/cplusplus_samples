#pragma once

#include "BinaryTree.h"

/**
* Binary Search Tree.
*/
class BinarySearchTree : public BinaryTree
{
private:
	// overload recursive function
	void insert(BTNode* pNode, int data);

public:
	void insert(int data);

	// runs associated tests (sample-run)
	static void RunTests();
};
