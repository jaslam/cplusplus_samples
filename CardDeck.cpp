// CardDeck.cpp

#include "stdafx.h"
#include <time.h>
#include <vector>
#include "CardDeck.h"

using namespace std;


const size_t CardDeck::TOTAL_CARDS_COUNT = 52;

void CardDeck::initializeCards()
{
	cout << endl << "empty deck" << endl << endl;

	// seed the random generator (again and again when re-filling the cards)
	srand((size_t)time(NULL));

	for (size_t i = 0; i < CardDeck::TOTAL_CARDS_COUNT; ++i) {
		m_vCards.push_back(i + 1);
	}
}

size_t CardDeck::getACard()
{
	// if card deck is empty, fill it again
	if (m_vCards.empty()) {
		initializeCards();
	}

	// get a random # from 0 to 51
	size_t idx = rand() % m_vCards.size();	// this size has to be current size of the vector

	// get a card at above random 
	vector<size_t>::iterator it = m_vCards.begin() + idx;

	size_t card = *it;	// save card before erasing it

	// erase the above card from the vector to have the remaining cards for next pull
	m_vCards.erase(it);

	// return the picked card
	return card;
}

void CardDeck::RunTests()
{
	cout << endl << ">>>>> Running CardDeck tests ...." << endl;
	CardDeck deck;
	for (size_t i = 0; i < 60; ++i) {
		cout << "card pulled: " << deck.getACard() << endl;
	}
}
