#include "stdafx.h"

#include <sstream>

#include "TowerOfHanoi.h"


using namespace std;

const string MyStack::toString()
{
	stringstream ss;

	ss << "[";
	stack<size_t> temp = *this;
	while (!temp.empty()) {
		ss << temp.top() << " ";
		temp.pop();
	}

	ss << "]";
	return ss.str();
}


MyStack::MyStack(size_t n)
{
	for (size_t i = n; i > 0; --i) {
		this->push(i);
	}
}

void TowerOfHanoi::Run(size_t n, MyStack& source, MyStack& destination, MyStack& spare)
{
	// base case
	if (n <= 0) {
		return;
	}

	// move 1 to n - 1 disks from source to spare using destination
	Run(n - 1, source, spare, destination);

	// move n disk from source to destination
	destination.push(source.top());
	cout << "Moved " << n << " from source: " << source.toString() << " to destination: " << destination.toString() << " (spare: " << spare.toString() << ")" << endl;
	source.pop();

	// move 1 to n - 1 disks from spare to destination using source
	Run(n - 1, spare, destination, source);
}

void TowerOfHanoi::RunTests()
{
	{
		cout << endl;
		MyStack source(3);
		MyStack destination;
		MyStack spare;

		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
		Run(source.size(), source, destination, spare);
		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
	}

	{
		cout << endl;
		MyStack source(1);
		MyStack destination;
		MyStack spare;

		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
		Run(source.size(), source, destination, spare);
		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
	}

	{
		cout << endl;
		MyStack source(2);
		MyStack destination;
		MyStack spare;

		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
		Run(source.size(), source, destination, spare);
		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
	}

	{
		cout << endl;
		MyStack source(4);
		MyStack destination;
		MyStack spare;

		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
		Run(source.size(), source, destination, spare);
		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
	}

	{
		cout << endl;
		MyStack source(10);
		MyStack destination;
		MyStack spare;

		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
		Run(source.size(), source, destination, spare);
		cout << "source: " << source.toString() << ", destination: " << destination.toString() << ", using: " << spare.toString() << endl;
	}
}
