// BinaryTree.cpp

#include "stdafx.h"

#include "BinaryTree.h"

#include <stack>

using namespace std;


void BinaryTree::preOrderTraversal(BTNode* pNode)
{
	if (pNode == NULL) {
		return;
	}

	cout << pNode->m_data << ", ";
	if (pNode->m_pLeft) {
		preOrderTraversal(pNode->m_pLeft);
	}
	if (pNode->m_pRight) {
		preOrderTraversal(pNode->m_pRight);
	}
}

void BinaryTree::preOrderTraversal()
{
	cout << endl << endl << "Pre-Order traversal:" << endl;
	preOrderTraversal(m_pRoot);
}

void BinaryTree::inOrderTraversal(BTNode* pNode)
{
	if (pNode == NULL) {
		return;
	}

	if (pNode->m_pLeft) {
		inOrderTraversal(pNode->m_pLeft);
	}
	cout << pNode->m_data << ", ";
	if (pNode->m_pRight) {
		inOrderTraversal(pNode->m_pRight);
	}
}

void BinaryTree::inOrderTraversal()
{
	cout << endl << endl << "In-Order traversal:" << endl;
	inOrderTraversal(m_pRoot);
}

void BinaryTree::postOrderTraversal(BTNode* pNode)
{
	if (pNode == NULL) {
		return;
	}

	if (pNode->m_pLeft) {
		postOrderTraversal(pNode->m_pLeft);
	}
	if (pNode->m_pRight) {
		postOrderTraversal(pNode->m_pRight);
	}
	cout << pNode->m_data << ", ";
}

void BinaryTree::postOrderTraversal()
{
	cout << endl << endl << "Post-Order traversal:" << endl;
	postOrderTraversal(m_pRoot);
}


void BinaryTree::preOrderTraversalIterative()
{
	cout << endl << endl << "Pre-Order traversal (iterative):" << endl;

	stack<BTNode*> st;
	BTNode* pNode = m_pRoot;

	while (!st.empty() || pNode != NULL) {
		if (pNode != NULL) {
			cout << pNode->m_data << ", ";
			if (pNode->m_pRight) {
				st.push(pNode->m_pRight);
			}
			pNode = pNode->m_pLeft;
		}
		else {
			pNode = st.top();
			st.pop();
		}
	}
}

void BinaryTree::inOrderTraversalIterative()
{
	cout << endl << endl << "In-Order traversal (iterative):" << endl;

	stack<BTNode*> st;
	BTNode* pNode = m_pRoot;

	while (!st.empty() || pNode != NULL) {
		if (pNode != NULL) {
			st.push(pNode);
			pNode = pNode->m_pLeft;
		}
		else {
			pNode = st.top();
			st.pop();
			cout << pNode->m_data << ", ";
			pNode = pNode->m_pRight;
		}
	}
}

void BinaryTree::postOrderTraversalIterative()
{
	cout << endl << endl << "Post-Order traversal (iterative):" << endl;

	stack<BTNode*> st;
	BTNode* pLastVisited = NULL;
	BTNode* pNode = m_pRoot;

	while (!st.empty() || pNode != NULL) {
		if (pNode != NULL) {
			st.push(pNode);
			pNode = pNode->m_pLeft;
		}
		else {
			BTNode* pPeek = st.top();
			if (pPeek->m_pRight != NULL && pLastVisited != pPeek->m_pRight) {
				pNode = pPeek->m_pRight;
			}
			else {
				cout << pPeek->m_data << ", ";
				pLastVisited = st.top();
				st.pop();
			}
		}
	}
}
