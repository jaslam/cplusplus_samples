// Strings.cpp

#include "stdafx.h"
#include "Strings.h"

using namespace std;

void Strings::RunTests()
{
	std::cout << endl << ">>>>> Running Strings tests ...." << endl;
	//Strings::printInterleavedStrings("A", "B");
	Strings::printInterleavedStrings("AB", "CD");


}

void Strings::printInterleavedStrings(const std::string s1, const std::string s2)
{
	std::cout << endl << "Interleaving strings \"" <<  s1 << "\" & \"" << s2 << "\" ...." << endl;

	const size_t len = s1.length() + s2.length() + 1;

	char* buffer = new char[len];
	memset(buffer, 0, len);

	printInterleavedStrings(s1.c_str(), s2.c_str(), buffer, len - 1);
	delete[] buffer;
}

void Strings::printInterleavedStrings(const char* s1, const char* s2, char* s, size_t len)
{
	static char * buffer = s;
	printf("\nFunction entry -> s1: %s, s2: %s, s: %s, buffer: %s", s1, s2, s, buffer);

	if (*s1 == '\0' && *s2 == '\0') {
		printf("\n$$$ %s\n", s - len);
	}

	if (*s1 != '\0') {
		*s = *s1;
		printInterleavedStrings(s1 + 1, s2, s + 1, len);
	}
	if (*s2 != '\0') {
		*s = *s2;
		printInterleavedStrings(s1, s2 + 1, s + 1, len);
	}

	printf("\nFunction return -> s1: %s, s2: %s, s: %s, buffer: %s", s1, s2, s, buffer);
}

