#pragma once
#include <string>
#include <vector>
#include <map>

// forward declaration
class TrieNode;

typedef std::map<char, TrieNode*> CHILD_NODES;

class TrieNode
{
public:
	char ch;
	bool isLastChar;
	CHILD_NODES childs;

	inline TrieNode(char ch, bool isLastChar = false)
	{
		this->ch = ch;
		this->isLastChar = isLastChar;
	}
};

class Trie
{
private:
	TrieNode* root;
	void words(TrieNode* node, std::vector<std::string>& results);
	TrieNode* findLast(TrieNode* node, std::string wordLike);

public:
	inline Trie() : root(NULL)
	{
	};
	~Trie();

	bool insert(const std::string& word);
	bool remove(const std::string& word);
	void print();
	const std::vector<std::string> suggest(const std::string& search);

	static void RunTests();
};

