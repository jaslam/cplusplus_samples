#pragma once;

#include <vector>

/**
 * CardDeck is a card-deck implementation, which provides random pulling of a card for playing the game.
 */
class CardDeck
{
private:
	static const size_t TOTAL_CARDS_COUNT;
	std::vector<size_t> m_vCards;
	void initializeCards();

public:
	// constructor
	CardDeck()
	{
		initializeCards();
	}

	// gets a card out of the remaining deck
	size_t getACard();

	// runs associated tests (sample-run)
	static void RunTests();
};
