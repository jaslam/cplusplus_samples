#include "stdafx.h"
#include "Trie.h"


using namespace std;

Trie::~Trie()
{
	// clean the Trie nodes
}

bool Trie::insert(const string& word)
{
	if (word.length() == 0) {
		return false;
	}

	TrieNode* node = NULL;
	if (root == NULL) {
		// first node is the root node of the Trie
		root = new TrieNode(word[0], (1 == word.length()));
	}
	else
	{
		if (word[0] != root->ch) {
			// first char should match with the root's char
			return false;
		}

	}

	TrieNode* current = root;
	for (size_t i = 1; i < word.length(); ++i) {
		CHILD_NODES::const_iterator it = current->childs.find(word[i]);
		
		if (it == current->childs.end()) {
			// child not yet found, create new node and link it to this node 
			current->childs[word[i]] = new TrieNode(word[i], (i == word.length() - 1));
		}

		// keep going to next node
		current = current->childs[word[i]];
	}

	return true;
}

bool Trie::remove(const string& word)
{
	return false;
}

void Trie::words(TrieNode* node, vector<string>& results)
{
	static string word = "";

	word.append(1, node->ch);
	if (node->isLastChar) {
		results.push_back(word);
	}

	// for every child in child-noeds, recursively call suggest again
	for (CHILD_NODES::const_iterator it = node->childs.begin(); it != node->childs.end(); ++it) {
		words(it->second, results);
		word.erase(word.length() - 1);
	}

	if (root == node) {
		word = "";
	}
}

TrieNode* Trie::findLast(TrieNode* node, string wordLike)
{
	if (wordLike.length() == 1 && wordLike[0] == node->ch) {
		return node;
	}
	if (wordLike.length() == 0) {
		return NULL;
	}
	
	// for every child in child-noeds, recursively call suggest again
	for (CHILD_NODES::const_iterator it = node->childs.begin(); it != node->childs.end(); ++it) {
		return findLast(it->second, wordLike.substr(1));
	}

	return NULL;
}

void Trie::print()
{
	cout << "\nTrie \"" << root->ch << "\" words: ";

	if (root == NULL) {
		cout << "\nTrie is empty" << endl;
		return;
	}

	vector<string> results;
	words(root, results);
	for (vector<string>::const_iterator it = results.begin(); it != results.end(); ++it) {
		cout << *it << "  ";
	}
};

const vector<string> Trie::suggest(const string& word)
{
	cout << "\nTrie \"" << word << "\" matching words: ";

	vector<string> results;

	if (root == NULL || word.empty() || root->ch != word[0]) {
		// cases: empty dictionary, empty word or first char not matching
		return results;
	}

	TrieNode* node = findLast(root, word);

	if (node != NULL) {
		words(node, results);

		string temp = word;
		temp.pop_back();
		for (vector<string>::const_iterator it = results.begin(); it != results.end(); ++it) {
			cout << temp << *it << "  ";
		}

	}


	return results;
}

void Trie::RunTests()
{
	{
		Trie dictN;
		string words[] = { "new", "news", "newbie", "newest", "newborn" };
		for (size_t i = 0; i < _countof(words); ++i) {
			dictN.insert(words[i]);
		}
		dictN.insert("no");
		dictN.print();
		dictN.suggest("newb");
	}

	{
		Trie dictI;
		string words[] = { "I", "Icecream", "Ice" };
		for (size_t i = 0; i < _countof(words); ++i) {
			dictI.insert(words[i]);
		}
		dictI.insert("Ink");
		dictI.print();
	}
}