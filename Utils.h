#pragma once

#include <iostream>
#include <string>

class Utils
{
public:

	static size_t StringLength(const char* str)
	{
		if (str == NULL) {
			return 0;
		}

		size_t i;
		for (i = 0; *(str + i) != '\0'; ++i) {
		}

		return i;
	}

	static char* ReverseString(char* str) {
		if (str == NULL || *str == '\0') {
			return str;
		}

		size_t i = 0, j = Utils::StringLength(str) - 1;

		// keep swapping till middle is reached
		char ch;
		while (i < j) {
			ch = str[j];
			str[j--] = str[i];
			str[i++] = ch;
		}

		return str;
	}

	static const char* Number2String(int number) {
		int n = number;

		if (n < 0) {
			n *= -1;
		}

		// keep finding 1 digit from the once place and put it in the char array
		char str[64] = {};
		size_t i = 0;
		int digit = 0;
		do {
			digit = n % 10;
			str[i++] = '0' + digit;

			n = n / 10;
		} while (n > 0);

		// reverse the str prepared so far
		std::string retStr = Utils::ReverseString(str);
		if (number < 0) {
			retStr.insert(0, "-");
		}

		std::cout << std::endl << "Number: " << number << ", String: " << retStr.c_str();

		return retStr.c_str();
	}

	static bool IsNumber(const char* str)
	{
		if (str == NULL || *str == '\0') {
			std::cout << std::endl << "Invalid number: <empty string>";
			return false;
		}

		char ch = '\0';
		bool decimalFound = false;

		for (size_t i = 0; *(str + i) != '\0'; ++i) {
			ch = *(str + i);

			// all digits are allowed at any position
			if (ch >= '0' && ch <= '9') {
				continue;
			}

			// '+' or '-' is allowed at 0'th position only
			if (i == 0 && (ch == '+' || ch == '-')) {
				continue;
			}

			// '.' is allowed at any position except at the last and only once
			if (ch == '.' && !decimalFound) {
				if (*(str + i + 1) != '\0') {
					decimalFound = true;
					continue;
				}
			}

			// else is not a number
			std::cout << std::endl << "Invalid number: " << str;
			return false;
		}
	
		std::cout << std::endl << "Valid number: " << str;
		return true;
	}


	static void RunTests()
	{
		std::cout << std::endl << std::endl << ">>>>> Running Utils tests ...." << std::endl;

		Utils::Number2String(-1567);
		Utils::Number2String(+567);
		Utils::Number2String(1);
		Utils::Number2String(0);


		// valid numbers
		Utils::IsNumber("1");
		Utils::IsNumber("-1");
		Utils::IsNumber("+1");
		Utils::IsNumber("-12.0");
		Utils::IsNumber(".1");
		Utils::IsNumber("+1434234325.450");

		// invalid numbers
		Utils::IsNumber("");
		Utils::IsNumber(NULL);
		Utils::IsNumber("!+1");
		Utils::IsNumber("abc");
		Utils::IsNumber("2.1.3");
		Utils::IsNumber("213.");
		Utils::IsNumber("2+1-3.");
		Utils::IsNumber("21.3.");
	}
};

