#pragma once


// singly linked list
template <class T>
struct SLLNode
{
	T m_data;
	SLLNode* m_pNext;

	SLLNode(T data) :
		m_data(data),
		m_pNext(NULL)
	{
	}
};

template <class T>
bool isCircularLinkedList(const SLLNode<T>* pHead)
{
	if (pHead == NULL) {
		cout << endl << "<empty>" << endl;
		return false;
	}

	SLLNode<T>* pSlow = const_cast<SLLNode<T>*>(pHead);	// slow ptr
	SLLNode<T>* pFast = pSlow->m_pNext;	// fast ptr (start ahead of the slow ptr)

	while (true) {
		if (!pFast || !pFast->m_pNext) {
			cout << endl << "<non-circular>" << endl;
			return false;
		}
		else if (pFast == pSlow || pFast->m_pNext == pSlow) {
			cout << endl << "<circular>" << endl;
			return true;
		}

		// advance pSlow by 1 step
		pSlow = pSlow->m_pNext;

		// advance pFast by 2 steps
		pFast = pFast->m_pNext->m_pNext;
	}
}


template <class T>
class SinglyLinkedList
{
private:
	SLLNode<T>* m_pHead;

public:
	SinglyLinkedList(SLLNode<T>* pHead = NULL) :
		m_pHead(pHead)
	{
	}

	void insertNode(T data);
	void print();
	const SLLNode<T>* getHead()
	{
		return m_pHead;
	}
	void reverse();
	//void findNode(T data);
	//void removeNode(T data);

	// runs associated tests (sample-run)
	static void RunTests();
};

template<class T>
void SinglyLinkedList<T>::insertNode(T data)
{
	SLLNode<T>* pNode = new SLLNode<T>(data);

	if (m_pHead == NULL) {
		m_pHead = pNode;
		return;
	}

	SLLNode<T>* pIt = m_pHead;
	while (pIt->m_pNext != NULL) {
		pIt = pIt->m_pNext;
	}

	pIt->m_pNext = pNode;
}

template<class T>
void SinglyLinkedList<T>::print()
{
	cout << endl << "Singly Linked List: " << endl;
	SLLNode<T>* pIt = m_pHead;
	while (pIt != NULL) {
		cout << pIt->m_data << ", ";
		pIt = pIt->m_pNext;
	}
}

template<class T>
void SinglyLinkedList<T>::reverse()
{
	// use 3 pointers to iterate the linked list
	SLLNode<T>* p1 = m_pHead;
	SLLNode<T>* p2 = p1 ? p1->m_pNext : p1;
	SLLNode<T>* p3 = NULL;

	if (p1 != p2) {
		// more than 1 node in the linked list, then make the head: the tail
		p1->m_pNext = NULL;
	}

	while (p2) {
		// p3 is the 3'rd node or NULL any time
		p3 = p2->m_pNext;

		// reverse the corrent node's next pointer
		p2->m_pNext = p1;

		// advanve these pointers
		p1 = p2;
		p2 = p3;
	}

	m_pHead = p1;
}

template<class T>
void SinglyLinkedList<T>::RunTests()
{
	cout << endl << endl << ">>>>> Running SinglyLinkedList tests ...." << endl;

	SinglyLinkedList<int> sll;
	sll.insertNode(1);
	sll.insertNode(2);
	sll.insertNode(3);
	sll.insertNode(4);
	sll.insertNode(5);
	sll.print();
	isCircularLinkedList(sll.getHead());
	sll.reverse();
	sll.print();
	isCircularLinkedList(sll.getHead());

	SinglyLinkedList<int> sll2;
	sll2.print();
	isCircularLinkedList(sll2.getHead());
	sll2.reverse();
	sll2.print();

	sll2.insertNode(24);
	sll2.print();
	isCircularLinkedList(sll2.getHead());
	sll2.reverse();
	sll2.print();
}



//
// circular linked list
//
template <class T>
class CircularLinkedList
{
private:
	SLLNode<T>* m_pHead;
	SLLNode<T>* m_pTail;

public:
	CircularLinkedList() :
		m_pHead(NULL),
		m_pTail(NULL)
	{
	}

	void insertNode(T data);
	void print();
	const SLLNode<T>* getHead()
	{
		return m_pHead;
	}

	static void RunTests();
};

template<class T>
void CircularLinkedList<T>::insertNode(T data)
{
	SLLNode<T>* pNode = new SLLNode<T>(data);

	if (m_pHead == NULL) {
		m_pHead = m_pTail = pNode;
		pNode->m_pNext = m_pHead;
		return;
	}

	m_pTail->m_pNext = pNode;
	m_pTail = pNode;
	m_pTail->m_pNext = m_pHead;
}

template<class T>
void CircularLinkedList<T>::print()
{
	cout << endl << endl << "Circular Linked List: " << endl;
	if (m_pHead == NULL) {
		cout << "<empty>" << endl;
		return;
	}

	SLLNode<T>* pIt = m_pHead;
	do {
		cout << pIt->m_data << ", ";
		pIt = pIt->m_pNext;
	} while (pIt != m_pHead);
}

template<class T>
void CircularLinkedList<T>::RunTests()
{
	cout << endl << endl << ">>>>> Running CircularLinkedList tests ...." << endl;

	// test normal case
	CircularLinkedList<int> cll;
	cll.insertNode(1);
	cll.insertNode(2);
	cll.insertNode(3);
	cll.insertNode(4);
	cll.insertNode(5);
	cll.insertNode(6);
	cll.insertNode(7);
	cll.print();

	isCircularLinkedList(cll.getHead());

	// test empty case
	CircularLinkedList<int> cll2;
	cll2.print();
	isCircularLinkedList(cll2.getHead());
	// test only one node
	cll2.insertNode('A');
	cll2.print();
	isCircularLinkedList(cll2.getHead());
}