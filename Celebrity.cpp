// Celebrity.cpp

#include "stdafx.h"
#include "Celebrity.h"
#include <string>


using namespace std;

class Person
{
public:
	std::string m_sName;
	bool m_bCelebrity;
	Person(const char* pszName, bool bCelebrity = false) :
		m_sName(pszName),
		m_bCelebrity(bCelebrity)
	{
	}
};

bool doYouKnow(const char* pszPerson1, const char* pszPerson2)
{
	if (strcmp(pszPerson1, "SRK") == 0) {
		return false;
	}

	return true;
}

const char* findCelebrity(const char ** ppPersons)
{
	for (int i = 0; ppPersons[i] != NULL; ++i) {

		if (!doYouKnow(ppPersons[i], ppPersons[i + 1])) {
			// current person doesn't know the next person, he/she is the celebrity (because the celebrity doesn't know any one)
			return ppPersons[i];
		}

		// current person knows the next person

		if (ppPersons[i + 2] == NULL) {
			// if only last two persons are left and the current persons knows the next person, that next person is the celebrity
			return ppPersons[i + 1];
		}
	}

	return "";
}

void Celebrity_RunTests()
{
	cout << endl << ">>>>> Running Celebrity tests ...." << endl;

	const char* persons[] = { "Alice", "Bob", "Peter", "Jack", "Tom", "Janet", "SRK", "Kim", NULL };
	cout << "Celebrity: " << findCelebrity(persons) << endl;
}

