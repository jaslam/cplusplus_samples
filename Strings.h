#pragma once
#include <iostream>
#include <string>

class Strings
{
public:
	static void RunTests();

	static void printInterleavedStrings(const std::string s1, const std::string s2);
	static void printInterleavedStrings(const char* s1, const char* s2, char* s, size_t len);
};
