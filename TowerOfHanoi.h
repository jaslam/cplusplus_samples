#pragma once

#include <stack>
#include <string>

class MyStack : public std::stack<size_t>
{
public: 
	MyStack(size_t n = 0);
	const std::string toString();
};

class TowerOfHanoi
{
private:
	static void Run(size_t n, MyStack& src, MyStack& use, MyStack& dest);

public:
	static void RunTests();
};

